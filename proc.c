#include "types.h"
#include "defs.h"
#include "param.h"
#include "memlayout.h"
#include "mmu.h"
#include "x86.h"
#include "proc.h"
#include "spinlock.h"
#include "date.h"

int generate_random_number(int ceil)
{
    static int t = 5354;
    int ret = t * 12531 % ceil;
    if (ret < 0)
        ret += ceil;
    return ret;
}

struct
{
    struct spinlock lock;
    struct proc proc[NPROC];
} ptable;

struct
{
    int current;
    int capacity;

    struct spinlock lock;
    struct spinlock init_lock;
} barrier;

static struct proc *initproc;

int nextpid = 1;
extern void forkret(void);
extern void trapret(void);

static void wakeup1(void *chan);

void pinit(void)
{
    initlock(&ptable.lock, "ptable");
}

void barrinit(void)
{
    initlock(&barrier.init_lock, "barrier init");
    initlock(&barrier.lock, "barrier");
}

// Must be called with interrupts disabled
int cpuid()
{
    return mycpu() - cpus;
}

// Must be called with interrupts disabled to avoid the caller being
// rescheduled between reading lapicid and running through the loop.
struct cpu *
mycpu(void)
{
    int apicid, i;

    if (readeflags() & FL_IF)
        panic("mycpu called with interrupts enabled\n");

    apicid = lapicid();
    // APIC IDs are not guaranteed to be contiguous. Maybe we should have
    // a reverse map, or reserve a register to store &cpus[i].
    for (i = 0; i < ncpu; ++i)
    {
        if (cpus[i].apicid == apicid)
            return &cpus[i];
    }
    panic("unknown apicid\n");
}

// Disable interrupts so that we are not rescheduled
// while reading proc from the cpu structure
struct proc *
myproc(void)
{
    struct cpu *c;
    struct proc *p;
    pushcli();
    c = mycpu();
    p = c->proc;
    popcli();
    return p;
}

//PAGEBREAK: 32
// Look in the process table for an UNUSED proc.
// If found, change state to EMBRYO and initialize
// state required to run in the kernel.
// Otherwise return 0.
static struct proc *
allocproc(void)
{
    struct proc *p;
    char *sp;

    acquire(&ptable.lock);

    for (p = ptable.proc; p < &ptable.proc[NPROC]; p++)
        if (p->state == UNUSED)
            goto found;

    release(&ptable.lock);
    return 0;

found:
    p->state = EMBRYO;
    p->pid = nextpid++;
    p->arrival_time = get_time();
    p->number_of_tickets = generate_random_number(1000);
    p->executed_cycle = 1;
    p->queue = 1;
    p->priority = 1.0;

    release(&ptable.lock);

    // Allocate kernel stack.
    if ((p->kstack = kalloc()) == 0)
    {
        p->state = UNUSED;
        return 0;
    }
    sp = p->kstack + KSTACKSIZE;

    // Leave room for trap frame.
    sp -= sizeof *p->tf;
    p->tf = (struct trapframe *)sp;

    // Set up new context to start executing at forkret,
    // which returns to trapret.
    sp -= 4;
    *(uint *)sp = (uint)trapret;

    sp -= sizeof *p->context;
    p->context = (struct context *)sp;
    memset(p->context, 0, sizeof *p->context);
    p->context->eip = (uint)forkret;

    return p;
}

//PAGEBREAK: 32
// Set up first user process.
void userinit(void)
{
    struct proc *p;
    extern char _binary_initcode_start[], _binary_initcode_size[];

    p = allocproc();

    initproc = p;
    if ((p->pgdir = setupkvm()) == 0)
        panic("userinit: out of memory?");
    inituvm(p->pgdir, _binary_initcode_start, (int)_binary_initcode_size);
    p->sz = PGSIZE;
    memset(p->tf, 0, sizeof(*p->tf));
    p->tf->cs = (SEG_UCODE << 3) | DPL_USER;
    p->tf->ds = (SEG_UDATA << 3) | DPL_USER;
    p->tf->es = p->tf->ds;
    p->tf->ss = p->tf->ds;
    p->tf->eflags = FL_IF;
    p->tf->esp = PGSIZE;
    p->tf->eip = 0; // beginning of initcode.S

    safestrcpy(p->name, "initcode", sizeof(p->name));
    p->cwd = namei("/");

    // this assignment to p->state lets other cores
    // run this process. the acquire forces the above
    // writes to be visible, and the lock is also needed
    // because the assignment might not be atomic.
    acquire(&ptable.lock);

    p->state = RUNNABLE;

    release(&ptable.lock);
}

// Grow current process's memory by n bytes.
// Return 0 on success, -1 on failure.
int growproc(int n)
{
    uint sz;
    struct proc *curproc = myproc();

    sz = curproc->sz;
    if (n > 0)
    {
        if ((sz = allocuvm(curproc->pgdir, sz, sz + n)) == 0)
            return -1;
    }
    else if (n < 0)
    {
        if ((sz = deallocuvm(curproc->pgdir, sz, sz + n)) == 0)
            return -1;
    }
    curproc->sz = sz;
    switchuvm(curproc);
    return 0;
}

// Create a new process copying p as the parent.
// Sets up stack to return as if from system call.
// Caller must set state of returned proc to RUNNABLE.
int fork(void)
{
    int i, pid;
    struct proc *np;
    struct proc *curproc = myproc();

    // Allocate process.
    if ((np = allocproc()) == 0)
    {
        return -1;
    }

    // Copy process state from proc.
    if ((np->pgdir = copyuvm(curproc->pgdir, curproc->sz)) == 0)
    {
        kfree(np->kstack);
        np->kstack = 0;
        np->state = UNUSED;
        return -1;
    }
    np->sz = curproc->sz;
    np->parent = curproc;
    *np->tf = *curproc->tf;

    // Clear %eax so that fork returns 0 in the child.
    np->tf->eax = 0;

    for (i = 0; i < NOFILE; i++)
        if (curproc->ofile[i])
            np->ofile[i] = filedup(curproc->ofile[i]);
    np->cwd = idup(curproc->cwd);

    safestrcpy(np->name, curproc->name, sizeof(curproc->name));

    pid = np->pid;

    acquire(&ptable.lock);

    np->state = RUNNABLE;

    release(&ptable.lock);

    return pid;
}

// Exit the current process.  Does not return.
// An exited process remains in the zombie state
// until its parent calls wait() to find out it exited.
void exit(void)
{
    struct proc *curproc = myproc();
    struct proc *p;
    int fd;

    if (curproc == initproc)
        panic("init exiting");

    // Close all open files.
    for (fd = 0; fd < NOFILE; fd++)
    {
        if (curproc->ofile[fd])
        {
            fileclose(curproc->ofile[fd]);
            curproc->ofile[fd] = 0;
        }
    }

    begin_op();
    iput(curproc->cwd);
    end_op();
    curproc->cwd = 0;

    acquire(&ptable.lock);

    // Parent might be sleeping in wait().
    wakeup1(curproc->parent);

    // Pass abandoned children to init.
    for (p = ptable.proc; p < &ptable.proc[NPROC]; p++)
    {
        if (p->parent == curproc)
        {
            p->parent = initproc;
            if (p->state == ZOMBIE)
                wakeup1(initproc);
        }
    }

    // Jump into the scheduler, never to return.
    curproc->state = ZOMBIE;
    sched();
    panic("zombie exit");
}

// Wait for a child process to exit and return its pid.
// Return -1 if this process has no children.
int wait(void)
{
    struct proc *p;
    int havekids, pid;
    struct proc *curproc = myproc();

    acquire(&ptable.lock);
    for (;;)
    {
        // Scan through table looking for exited children.
        havekids = 0;
        for (p = ptable.proc; p < &ptable.proc[NPROC]; p++)
        {
            if (p->parent != curproc)
                continue;
            havekids = 1;
            if (p->state == ZOMBIE)
            {
                // Found one.
                pid = p->pid;
                kfree(p->kstack);
                p->kstack = 0;
                freevm(p->pgdir);
                p->pid = 0;
                p->parent = 0;
                p->name[0] = 0;
                p->killed = 0;
                p->state = UNUSED;
                release(&ptable.lock);
                return pid;
            }
        }

        // No point waiting if we don't have any children.
        if (!havekids || curproc->killed)
        {
            release(&ptable.lock);
            return -1;
        }

        // Wait for children to exit.  (See wakeup1 call in proc_exit.)
        sleep(curproc, &ptable.lock); //DOC: wait-sleep
    }
}

float calculate_hrrn(struct proc *p)
{
    uint current_time = get_time();
    uint waiting_time = current_time - p->arrival_time;
    return waiting_time / p->executed_cycle;
}

struct proc *next_process()
{
    struct proc *p;
    int current_queue = 1;
    if (current_queue == 1)
    {

        int total_tickets = 0;
        for (p = ptable.proc; p < &ptable.proc[NPROC]; p++)
        {
            if (p->queue == current_queue && p->state == RUNNABLE)
            {
                total_tickets += p->number_of_tickets;
            }
        }

        if (total_tickets)
        {
            int rnd = generate_random_number(total_tickets);

            for (p = ptable.proc; p < &ptable.proc[NPROC]; p++)
            {
                if (p->queue != current_queue || p->state != RUNNABLE)
                    continue;

                rnd -= p->number_of_tickets;
                if (rnd <= 0)
                {
                    return p;
                }
            }
        }
        current_queue = 2;
    }
    if (current_queue == 2)
    {
        float max_hrrn = 0;
        struct proc *max_hrrn_p = 0;

        for (p = ptable.proc; p < &ptable.proc[NPROC]; p++)
        {
            if (p->queue != current_queue || p->state != RUNNABLE)
                continue;

            float hrrn = calculate_hrrn(p);
            if (hrrn > max_hrrn || !hrrn)
            {
                max_hrrn = hrrn;
                max_hrrn_p = p;
            }
        }
        if (max_hrrn_p)
            return max_hrrn_p;

        current_queue = 3;
    }
    if (current_queue == 3)
    {
        float min_priority = 200000;
        struct proc *min_priority_p = 0;
        for (p = ptable.proc; p < &ptable.proc[NPROC]; p++)
        {
            if (p->queue != current_queue || p->state != RUNNABLE)
                continue;

            if (p->priority < min_priority)
            {
                min_priority = p->priority;
                min_priority_p = p;
            }
        }

        if (min_priority_p)
        {
            if (min_priority_p->priority - 0.10f >= 0)
                min_priority_p->priority = min_priority_p->priority - 0.1;
            if (min_priority_p->priority < 0.1)
                min_priority_p->priority = 0.0;
            return min_priority_p;
        }
    }
    return 0;
}

//PAGEBREAK: 42
// Per-CPU process scheduler.
// Each CPU calls scheduler() after setting itself up.
// Scheduler never returns.  It loops, doing:
//  - choose a process to run
//  - swtch to start running that process
//  - eventually that process transfers control
//      via swtch back to the scheduler.
void scheduler(void)
{
    struct proc *p;
    struct cpu *c = mycpu();
    c->proc = 0;

    for (;;)
    {
        // Enable interrupts on this processor.
        sti();

        // Loop over process table looking for process to run.
        acquire(&ptable.lock);
        for (p = ptable.proc; p < &ptable.proc[NPROC]; p++)
        {
            if (p->state != RUNNABLE)
                continue;

            // Switch to chosen process.  It is the process's job
            // to release ptable.lock and then reacquire it
            // before jumping back to us.
            c->proc = p;
            switchuvm(p);
            p->state = RUNNING;

            swtch(&(c->scheduler), p->context);
            switchkvm();

            // Process is done running for now.
            // It should have changed its p->state before coming back.
            c->proc = 0;
        }
        release(&ptable.lock);
    }
}

// Enter scheduler.  Must hold only ptable.lock
// and have changed proc->state. Saves and restores
// intena because intena is a property of this
// kernel thread, not this CPU. It should
// be proc->intena and proc->ncli, but that would
// break in the few places where a lock is held but
// there's no process.
void sched(void)
{
    int intena;
    struct proc *p = myproc();

    if (!holding(&ptable.lock))
        panic("sched ptable.lock");
    if (mycpu()->ncli != 1)
        panic("sched locks");
    if (p->state == RUNNING)
        panic("sched running");
    if (readeflags() & FL_IF)
        panic("sched interruptible");
    intena = mycpu()->intena;
    swtch(&p->context, mycpu()->scheduler);
    mycpu()->intena = intena;
}

// Give up the CPU for one scheduling round.
void yield(void)
{
    acquire(&ptable.lock); //DOC: yieldlock
    myproc()->state = RUNNABLE;
    sched();
    release(&ptable.lock);
}

// A fork child's very first scheduling by scheduler()
// will swtch here.  "Return" to user space.
void forkret(void)
{
    static int first = 1;
    // Still holding ptable.lock from scheduler.
    release(&ptable.lock);

    if (first)
    {
        // Some initialization functions must be run in the context
        // of a regular process (e.g., they call sleep), and thus cannot
        // be run from main().
        first = 0;
        iinit(ROOTDEV);
        initlog(ROOTDEV);
    }

    // Return to "caller", actually trapret (see allocproc).
}

// Atomically release lock and sleep on chan.
// Reacquires lock when awakened.
void sleep(void *chan, struct spinlock *lk)
{
    struct proc *p = myproc();

    if (p == 0)
        panic("sleep");

    if (lk == 0)
        panic("sleep without lk");

    // Must acquire ptable.lock in order to
    // change p->state and then call sched.
    // Once we hold ptable.lock, we can be
    // guaranteed that we won't miss any wakeup
    // (wakeup runs with ptable.lock locked),
    // so it's okay to release lk.
    if (lk != &ptable.lock)
    {                          //DOC: sleeplock0
        acquire(&ptable.lock); //DOC: sleeplock1
        release(lk);
    }
    // Go to sleep.
    p->chan = chan;
    p->state = SLEEPING;

    sched();

    // Tidy up.
    p->chan = 0;

    // Reacquire original lock.
    if (lk != &ptable.lock)
    { //DOC: sleeplock2
        release(&ptable.lock);
        acquire(lk);
    }
}

//PAGEBREAK!
// Wake up all processes sleeping on chan.
// The ptable lock must be held.
static void
wakeup1(void *chan)
{
    struct proc *p;

    for (p = ptable.proc; p < &ptable.proc[NPROC]; p++)
        if (p->state == SLEEPING && p->chan == chan)
            p->state = RUNNABLE;
}

// Wake up all processes sleeping on chan.
void wakeup(void *chan)
{
    acquire(&ptable.lock);
    wakeup1(chan);
    release(&ptable.lock);
}

// Kill the process with the given pid.
// Process won't exit until it returns
// to user space (see trap in trap.c).
int kill(int pid)
{
    struct proc *p;

    acquire(&ptable.lock);
    for (p = ptable.proc; p < &ptable.proc[NPROC]; p++)
    {
        if (p->pid == pid)
        {
            p->killed = 1;
            // Wake process from sleep if necessary.
            if (p->state == SLEEPING)
                p->state = RUNNABLE;
            release(&ptable.lock);
            return 0;
        }
    }
    release(&ptable.lock);
    return -1;
}

//PAGEBREAK: 36
// Print a process listing to console.  For debugging.
// Runs when user types ^P on console.
// No lock to avoid wedging a stuck machine further.
static char *states[] = {
    [UNUSED] "unused",
    [EMBRYO] "embryo",
    [SLEEPING] "sleep ",
    [RUNNABLE] "runble",
    [RUNNING] "run   ",
    [ZOMBIE] "zombie",
};

void procdump(void)
{
    int i;
    struct proc *p;
    char *state;
    uint pc[10];

    for (p = ptable.proc; p < &ptable.proc[NPROC]; p++)
    {
        if (p->state == UNUSED)
            continue;
        if (p->state >= 0 && p->state < NELEM(states) && states[p->state])
            state = states[p->state];
        else
            state = "???";
        cprintf("%d %s %s", p->pid, state, p->name);
        if (p->state == SLEEPING)
        {
            getcallerpcs((uint *)p->context->ebp + 2, pc);
            for (i = 0; i < 10 && pc[i] != 0; i++)
                cprintf(" %p", pc[i]);
        }
        cprintf("\n");
    }
}

int count_num_of_digits(int num)
{
    int count = 1;
    while (num /= 10)
        count++;
    return count;
}

int get_children_recursive(int parent_pid)
{
    int res = 0;
    struct proc *p;
    for (p = ptable.proc; p < &ptable.proc[NPROC]; p++)
    {
        if (p->parent->pid == parent_pid)
        {
            int pid = p->pid;
            int count = count_num_of_digits(pid);
            for (int i = 0; i < count; i++)
                res *= 10;
            res += pid;

            int grandChildren = get_children_recursive(pid);
            if (grandChildren)
            {
                count = count_num_of_digits(grandChildren);
                for (int i = 0; i < count; i++)
                    res *= 10;
                res += grandChildren;
            }
        }
    }
    return res;
}

int get_children(int parent_pid)
{
    acquire(&ptable.lock);
    int res = get_children_recursive(parent_pid);
    release(&ptable.lock);
    return res;
}

uint convert_rtcdate_to_sec(struct rtcdate dt)
{
    // return dt.year * 60 * 60 * 24 * 30 * 12 +
    //        dt.month * 60 * 60 * 24 * 30 +
    return dt.day * 60 * 60 * 24 +
           dt.hour * 60 * 60 +
           dt.minute * 60 +
           dt.second;
}

uint get_time(void)
{
    struct rtcdate dt;
    cmostime(&dt);
    return convert_rtcdate_to_sec(dt);
}

int print_all_process_info(void)
{
    cprintf("\nname \t pid \t state \t queue \t priority \t tickets \t cycles \t HRRN \t arrival\n");
    struct proc *p;
    char *state;
    char priority[256];
    char hrrn_str[256];
    for (p = ptable.proc; p < &ptable.proc[NPROC]; p++)
    {
        if (p->state == UNUSED)
            continue;

        ftos(p->priority, priority);
        ftos(calculate_hrrn(p), hrrn_str);
        if (p->state >= 0 && p->state < NELEM(states) && states[p->state])
            state = states[p->state];
        else
            state = "???";

        cprintf("%s \t %d \t %s  %d \t %s \t\t %d \t\t %d \t\t %s \t %d\n",
                p->name,
                p->pid,
                state,
                p->queue,
                priority,
                p->number_of_tickets,
                p->executed_cycle,
                hrrn_str,
                p->arrival_time);
    }
    return 0;
}

struct proc *find_process(int pid)
{
    struct proc *p;
    for (p = ptable.proc; p < &ptable.proc[NPROC]; p++)
    {
        if (p->pid == pid)
        {
            return p;
        }
    }
    return 0;
}

int change_queue(int pid, int new_queue)
{
    struct proc *p;
    if (!(p = find_process(pid)))
    {
        return -1;
    }
    if (new_queue < 1 || new_queue > 3)
    {
        return -2;
    }
    p->queue = new_queue;
    return 0;
}

int change_ticket_number(int pid, int new_ticket_number)
{
    struct proc *p;
    if (!(p = find_process(pid)))
    {
        return -1;
    }
    p->number_of_tickets = new_ticket_number;
    return 0;
}

int change_priority(int pid, float new_priority)
{
    struct proc *p;
    if (!(p = find_process(pid)))
    {
        return -1;
    }
    p->priority = new_priority;
    return 0;
}

void rec(int i)
{
    if (!i)
        return;

    acquireReetrant(&ptable.lock);
    rec(i - 1);
}

int test_reentrant_acquire()
{
    acquireReetrant(&ptable.lock);
    rec(5);
    release(&ptable.lock);
    return 0;
}

int barrier_init(int count)
{
    // if (holding(&barrier.init_lock))
    //     panic("barrier_init not allowed when holding barrier.init_lock");

    acquire(&barrier.init_lock);
    if (barrier.capacity > 0)
    {
        release(&barrier.init_lock);
        cprintf("barrier init: fail\n");
        return -1;
    }

    barrier.capacity = count;
    barrier.current = 0;

    release(&barrier.init_lock);

    cprintf("barrier initiated with total count of %d\n", count);
    return 0;
}

int barrier_wait()
{
    int currPid = myproc()->pid;
    acquire(&barrier.lock);

    barrier.current++;

    cprintf("process %d arrived at barrier | curr process in: %d | capacity : %d\n",
            currPid, barrier.current, barrier.capacity);

    if (barrier.current == barrier.capacity)
    {
        cprintf("\nevery one passed the barrier | PIDs: ");
        for (struct proc *p = ptable.proc; p < &ptable.proc[NPROC]; p++)
            if (p->state == SLEEPING && p->chan == &barrier)
                cprintf("%d, ", p->pid);
        cprintf("%d\n", currPid);

        wakeup(&barrier);
    }
    else
    {
        sleep(&barrier, &barrier.lock);
    }

    release(&barrier.lock);
    return 0;
}

int barrier_destroy()
{
    acquire(&barrier.init_lock);

    wakeup(&barrier);
    barrier.capacity = 0;

    release(&barrier.init_lock);
    cprintf("barrier destroyed\n");
    return 0;
}
