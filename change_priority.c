#include "types.h"
#include "stat.h"
#include "user.h"

int main(int argc, char **argv)
{
    if(argc < 3)
    {
        printf(2, "usage: change_priority pid new_priority\n");
        exit();
    }

    return -1 * change_priority(atoi(argv[1]), argv[2]);
}
