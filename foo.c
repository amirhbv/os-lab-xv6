#include "types.h"
#include "stat.h"
#include "user.h"

int main(void)
{
    int main_pid = getpid();
    int pid;

    pid = fork();
    if (pid < 0)
    {
        printf(2, "fork failed\n");
        exit();
    }
    if (pid == 0)
    {
        delay(15);
        exit();
    }
    else
    {
        delay(5);
        int pid2 = fork();
        if (pid2 < 0)
        {
            printf(2, "fork failed\n");
            exit();
        }
        if (pid2 == 0)
        {
            delay(8);

            exit();
        }
        else
        {
            delay(3);
            change_queue(main_pid, 2);
            change_queue(pid, 3);
            change_queue(pid2, 2);
            wait();
        }
        wait();
    }

    exit();
}
