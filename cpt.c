#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"

void cpt(int inputFd, int outputFd)
{
    char buf[512];
    int n;

    while ((n = read(inputFd, buf, sizeof(buf))) > 0)
    {
        if (write(outputFd, buf, n) != n)
        {
            printf(1, "cpt: write error\n");
            exit();
        }
        if (inputFd == 0) // read until \n from stdin
        {
            break;
        }
    }
    if (n < 0)
    {
        printf(1, "cpt: read error\n");
        exit();
    }
}

int main(int argc, char *argv[])
{
    int inputFd, outputFd;

    if (argc == 2)
    {
        inputFd = 0;

        unlink(argv[1]);
        if ((outputFd = open(argv[1], O_CREATE | O_WRONLY)) < 0)
        {
            printf(1, "cpt: cannot open %s\n", argv[1]);
            exit();
        }
    }
    else if (argc == 3)
    {
        if ((inputFd = open(argv[1], O_RDONLY)) < 0)
        {
            printf(1, "cpt: cannot open %s\n", argv[1]);
            exit();
        }

        unlink(argv[2]);
        if ((outputFd = open(argv[2], O_CREATE | O_WRONLY)) < 0)
        {
            printf(1, "cpt: cannot open %s\n", argv[2]);
            close(inputFd);
            exit();
        }
    }
    else
    {
        printf(2, "cpt:\n\tusage: cpt [/path/to/input/file] /path/to/output/file\n");
        exit();
    }

    cpt(inputFd, outputFd);

    if (inputFd)
    {
        close(inputFd);
    }
    close(outputFd);

    exit();
}
