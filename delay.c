#include "types.h"
#include "stat.h"
#include "user.h"

int main(int argc, char **argv)
{
    if(argc < 2)
    {
        printf(2, "usage: delay sec\n");
        exit();
    }

    uint start = get_time();
    delay(atoi(argv[1]));
    uint end = get_time();

    printf(1, "difference between start and end: %ds\n", end - start);
    exit();
}
