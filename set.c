#include "types.h"
#include "stat.h"
#include "user.h"

int main(int argc, char **argv)
{
    if(argc != 3)
    {
        printf(2, "set:\n\tusage: set ENV_VAR new_value\n");
        exit();
    }

    if (strcmp("PATH", argv[1]) == 0)
    {
        set_path(argv[2]);
    }
    else
    {
        printf(2, "Unknown ENV_VAR: %s\nusage: set ENV_VAR new_value\n", argv[1]);
    }

    exit();
}
