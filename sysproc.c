#include "types.h"
#include "x86.h"
#include "defs.h"
#include "param.h"
#include "memlayout.h"
#include "mmu.h"
#include "proc.h"

int sys_fork(void)
{
    return fork();
}

int sys_exit(void)
{
    exit();
    return 0; // not reached
}

int sys_wait(void)
{
    return wait();
}

int sys_kill(void)
{
    int pid;

    if (argint(0, &pid) < 0)
        return -1;
    return kill(pid);
}

int sys_getpid(void)
{
    return myproc()->pid;
}

int sys_sbrk(void)
{
    int addr;
    int n;

    if (argint(0, &n) < 0)
        return -1;
    addr = myproc()->sz;
    if (growproc(n) < 0)
        return -1;
    return addr;
}

int sys_sleep(void)
{
    int n;
    uint ticks0;

    if (argint(0, &n) < 0)
        return -1;
    acquire(&tickslock);
    ticks0 = ticks;
    while (ticks - ticks0 < n)
    {
        if (myproc()->killed)
        {
            release(&tickslock);
            return -1;
        }
        sleep(&ticks, &tickslock);
    }
    release(&tickslock);
    return 0;
}

// return how many clock tick interrupts have occurred
// since start.
int sys_uptime(void)
{
    uint xticks;

    acquire(&tickslock);
    xticks = ticks;
    release(&tickslock);
    return xticks;
}

int sys_count_num_of_digits(void)
{
    int num;
    asm volatile("movl %%esi, %0;"
                    : "=r"(num)
                    :
                    : "%esi");

    return count_num_of_digits(num);
}

int sys_get_parent_id(void)
{
    return myproc()->parent->pid;
}

int sys_get_children(void)
{
    int pid;
    if (argint(0, &pid) < 0)
        return -1;

    return get_children(pid);
}

#define TICK_PER_SECOND 100 // every tick is 10ns

int sys_delay(void)
{
    int sec;
    if (argint(0, &sec) < 0)
        return -1;

    uint ticks0 = ticks;
    while (ticks - ticks0 < sec * TICK_PER_SECOND)
    {
        if (myproc()->killed)
        {
            return -1;
        }
        sti();
    }
    return 0;
}

int sys_get_time(void)
{
    return get_time();
}

int sys_print_all_process_info(void)
{
    return print_all_process_info();
}

int sys_change_queue(void)
{
    int pid;
    if (argint(0, &pid) < 0)
        return -1;

    int new_queue;
    if (argint(1, &new_queue) < 0)
        return -1;

    return change_queue(pid, new_queue);
}

int sys_change_ticket_number(void)
{
    int pid;
    if (argint(0, &pid) < 0)
        return -1;

    int new_ticket_number;
    if (argint(1, &new_ticket_number) < 0)
        return -1;

    return change_ticket_number(pid, new_ticket_number);
}

int sys_change_priority(void)
{
    int pid;
    if (argint(0, &pid) < 0)
        return -1;

    char *new_priority;
    if (argstr(1, &new_priority) < 0)
        return -1;

    return change_priority(pid, stof(new_priority));
}

int sys_test_reentrant_acquire(void)
{
    return test_reentrant_acquire();
}

int sys_barrier_init(void)
{
    int count;
    if (argint(0, &count) < 0)
        return -1;

    return barrier_init(count);
}

int sys_barrier_wait(void)
{
    return barrier_wait();
}

int sys_barrier_destroy(void)
{
    return barrier_destroy();
}
