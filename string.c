#include "types.h"
#include "x86.h"

void *
memset(void *dst, int c, uint n)
{
    if ((int)dst % 4 == 0 && n % 4 == 0)
    {
        c &= 0xFF;
        stosl(dst, (c << 24) | (c << 16) | (c << 8) | c, n / 4);
    }
    else
        stosb(dst, c, n);
    return dst;
}

int memcmp(const void *v1, const void *v2, uint n)
{
    const uchar *s1, *s2;

    s1 = v1;
    s2 = v2;
    while (n-- > 0)
    {
        if (*s1 != *s2)
            return *s1 - *s2;
        s1++, s2++;
    }

    return 0;
}

void *
memmove(void *dst, const void *src, uint n)
{
    const char *s;
    char *d;

    s = src;
    d = dst;
    if (s < d && s + n > d)
    {
        s += n;
        d += n;
        while (n-- > 0)
            *--d = *--s;
    }
    else
        while (n-- > 0)
            *d++ = *s++;

    return dst;
}

// memcpy exists to placate GCC.  Use memmove.
void *
memcpy(void *dst, const void *src, uint n)
{
    return memmove(dst, src, n);
}

int strncmp(const char *p, const char *q, uint n)
{
    while (n > 0 && *p && *p == *q)
        n--, p++, q++;
    if (n == 0)
        return 0;
    return (uchar)*p - (uchar)*q;
}

char *
strncpy(char *s, const char *t, int n)
{
    char *os;

    os = s;
    while (n-- > 0 && (*s++ = *t++) != 0)
        ;
    while (n-- > 0)
        *s++ = 0;
    return os;
}

// Like strncpy but guaranteed to NUL-terminate.
char *
safestrcpy(char *s, const char *t, int n)
{
    char *os;

    os = s;
    if (n <= 0)
        return os;
    while (--n > 0 && (*s++ = *t++) != 0)
        ;
    *s = 0;
    return os;
}

int strlen(const char *s)
{
    int n;

    for (n = 0; s[n]; n++)
        ;
    return n;
}

char *strcat(char *s1, const char *s2)
{
    char *b = s1;

    while (*s1)
        ++s1;
    while (*s2)
        *s1++ = *s2++;
    *s1 = 0;

    return b;
}

float stof(const char *s)
{
    float res = 0, sign = 1;
    if (*s == '-')
    {
        s++;
        sign = -1;
    };
    for (int point_seen = 0; *s; s++)
    {
        if (*s == '.')
        {
            point_seen = 1;
            continue;
        };
        int d = *s - '0';
        if (d >= 0 && d <= 9)
        {
            if (point_seen)
                sign /= 10.0f;
            res = res * 10.0f + (float)d;
        };
    };
    return res * sign;
}

void ftos(float num, char *res)
{
    int integer_part = num, power_number = 1;
    int intSize = 0;
    num = num - integer_part;
    for (int i = 0;; i++)
    {
        if (integer_part < power_number)
            break;
        else
        {
            intSize++;
            power_number *= 10;
        }
    }

    int stringIndex = 0;
    if (intSize == 0)
        res[stringIndex++] = '0';
    for (int i = 0; i < intSize; i++)
    {
        power_number = power_number / 10;
        res[stringIndex++] = (int)(integer_part / power_number) + '0';
        integer_part = integer_part - ((int)(integer_part / power_number)) * power_number;
    }
    if (num == 0)
    {
        res[stringIndex++] = '\0';
        return;
    }
    res[stringIndex++] = '.';
    for (int i = 0; i < 2; i++)
    {
        if (num == 0)
            break;
        num = num * 10;
        integer_part = num;
        res[stringIndex++] = (int)(integer_part) + '0';
        num = num - integer_part;
    }
    res[stringIndex++] = 0;
    return;
}
