#include "types.h"
#include "stat.h"
#include "user.h"

int main(int argc, char **argv)
{
    if (argc < 2)
    {
        printf(2, "usage: count num ...\n");
        exit();
    }

    int prevVal;
    asm volatile("movl %%esi, %0;"
                    : "=r"(prevVal)
                    :
                    : "%esi");

    for (int i = 1; i < argc; i++)
    {
        int num = atoi(argv[i]);
        asm volatile("movl %0, %%esi;"
                        :
                        : "r"(num)
                        : "%esi");

        printf(1, "%d has %d digit(s)\n", num, count_num_of_digits());
    }

    asm volatile("movl %0, %%esi;"
                    :
                    : "r"(prevVal)
                    : "%esi");
    exit();
}
